Source for Web Services course
==============================

This repository contains the source code for a project from The Technical University of Denmark. 
This was developed for the Software Development of Web Services E14 course.

This contains SOAP based web services, BPEL and REST implementations.

Known issues (1st December, 2014)
---------------------------------

In the RESTFul implementation of TravelGood, we have not been able to
to succesfully transfer an object of type CreditCardWrapper. This makes a lot of the 
test cases unable to succeed because the CreditCardInfo is necessary to book or 
cancel a flight or hotel. We have not been able to fix this YET due to an internal server
error that we have found hard to interpret. However we aim to have a fault FREE RESTFul
implementation working at the examination and be able to explain what caused this internal
server error to begin with. Including what we had to do to fix it. 

The BPEL implementation of TravelGood runs successfully for test cases P1, P2, B and C1. 
Test case C2, however, is not implemented, as the current design of the system prevents 
the BPEL process from detecting errors in cancellation. 
Furthermore, the BPEL process will not terminate by itself as specified, as the use of
the necessary OnAlarm event resulted in errors that propagated throughout the application.
We therefore chose to leave out this feature, and allow the test cases to run without
problems.


package dk.dtu.ws.niceview;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.5-b05 
 * Generated source version: 2.1
 * 
 */
@WebService(name = "NiceViewWSDLPortType", targetNamespace = "http://dk.dtu.ws.niceview")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface NiceViewWSDLPortType {


    /**
     * 
     * @param request
     * @return
     *     returns dk.dtu.ws.niceview.HotelsType
     */
    @WebMethod
    @WebResult(name = "getHotelsResponse", targetNamespace = "http://dk.dtu.ws.niceview.types", partName = "hotels")
    public HotelsType getHotels(
        @WebParam(name = "hotelQuery", targetNamespace = "http://dk.dtu.ws.niceview.types", partName = "request")
        HotelQuery request);

    /**
     * 
     * @param request
     * @return
     *     returns boolean
     * @throws BookHotelFaultType
     */
    @WebMethod
    @WebResult(name = "bookHotelResponse", targetNamespace = "http://dk.dtu.ws.niceview.types", partName = "booked")
    public boolean bookHotel(
        @WebParam(name = "bookHotelRequest", targetNamespace = "http://dk.dtu.ws.niceview", partName = "request")
        BookHotelRequest request)
        throws BookHotelFaultType
    ;

    /**
     * 
     * @param request
     */
    @WebMethod
    @Oneway
    public void cancelHotel(
        @WebParam(name = "cancelHotelRequest", targetNamespace = "http://dk.dtu.ws.niceview.types", partName = "request")
        CancelHotelRequest request);

}

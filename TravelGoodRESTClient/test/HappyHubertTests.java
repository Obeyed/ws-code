/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import travelgood.ws.entities.CreditCardWrapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import dk.dtu.ws.lameduck.AirportType;
import dk.dtu.ws.lameduck.CreditCardInfoType;
import dk.dtu.ws.lameduck.ExpirationDateType;
import dk.dtu.ws.lameduck.FlightInfo;
import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.lameduck.FlightPathType;
import dk.dtu.ws.lameduck.FlightsQuery;
import dk.dtu.ws.niceview.HotelQuery;
import dk.dtu.ws.niceview.HotelInfo;
import dk.dtu.ws.niceview.HotelType;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import travelgood.ws.itineraryprocess.representation.FlightRepresentation;
import travelgood.ws.itineraryprocess.representation.HotelRepresentation;
import travelgood.ws.itineraryprocess.representation.ItineraryRepresentation;
import travelgood.ws.itineraryprocess.representation.StatusRepresentation;

/**
 *
 * @author jcfinnerup
 */
public class HappyHubertTests {
    
    static final String MEDIATYPE_ITINERARYPROCESS = "application/itineraryprocess+xml"; 
    Client client;
    String baseURI;
    WebResource itineraries;
    
    /** For Search Purposes **/
    private static final Map<String, String> airportToCodes;
    static
    {
        airportToCodes = new HashMap<String, String>();
        airportToCodes.put("Copenhagen Airport", "CPH");
        airportToCodes.put("Athens International Airport", "ATH");
    }
    
    /** Constructor **/
    public HappyHubertTests() {
        client = Client.create();
        baseURI = "http://localhost:8080/TravelGoodREST";
        itineraries = client.resource(baseURI).path("itineraries");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
   
    /** TEST P1 - Planning and Booking. **/
    @Test
    public void testP1() throws DatatypeConfigurationException{
        String cid = "1";
        String iid = "henning";
        StatusRepresentation result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
                accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);

	 			
        long dateLong = 1416246128000L;
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(dateLong);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
       
		/* Get list of flights. */ 
        FlightsQuery query = createFlightsQuery(xmlCal, "Copenhagen Airport", "Athens International Airport");
        FlightRepresentation flightResult = getFlightsCall(query, cid, iid);
        FlightInfoType flight = flightResult.getFlights().getFlightInfo().get(0);
        FlightInfo flightInfo = new FlightInfo();
        flightInfo.setFlightInfo(flight);
        String flightPath1 = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/addflight";
		/* Add first flight. */
        StatusRepresentation flightResult1 = client.resource(flightPath1).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, flightInfo);
        
        long arrivalLong = 1418674616000L;
        GregorianCalendar arrivalCal = new GregorianCalendar();
        Date arrivalDate = new Date(arrivalLong);
        arrivalCal.setTime(arrivalDate);
        XMLGregorianCalendar arrivalXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(arrivalCal);
        
        long departureLong = 1419279416000L;
        GregorianCalendar departureCal = new GregorianCalendar();
        Date departureDate = new Date(departureLong);
        departureCal.setTime(departureDate);
        XMLGregorianCalendar departureXMLCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(departureCal);
       
	/* Get list of hotels. */	 
        HotelQuery hotelRequest1 = new HotelQuery();
        hotelRequest1.setCity("Copenhagen");
        hotelRequest1.setArrival(arrivalXMLCal);
        hotelRequest1.setDeparture(departureXMLCal);  
        
        HotelRepresentation hotelResult1 = getHotelsCall(hotelRequest1, cid, iid);
        HotelType hotel = hotelResult1.getHotels().getHotel().get(0);
        HotelInfo hotelInfo = new HotelInfo();
        
        hotelInfo.setHotel(hotel);
        String hotelPath1 = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/addhotel";
	
        /* Add hotel. */
        StatusRepresentation hotelStatus1 = client.resource(hotelPath1).accept(MEDIATYPE_ITINERARYPROCESS).
              type(MEDIATYPE_ITINERARYPROCESS).
              put(StatusRepresentation.class, hotelInfo);
          
        /** Adding two flights (same data for testing purposes) **/
		/* Add second flight. */
        StatusRepresentation flightResult2 = client.resource(flightPath1).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, flightInfo);
        /* Add third flight. */ 
        StatusRepresentation flightResult3 = client.resource(flightPath1).accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class, flightInfo);
        
        /** Adding last hotel (same data for testing purpose)**/
		/* Add second hotel. */
        StatusRepresentation hotelStatus2 = client.resource(hotelPath1).accept(MEDIATYPE_ITINERARYPROCESS).
              type(MEDIATYPE_ITINERARYPROCESS).
              put(StatusRepresentation.class, hotelInfo);
        
        /** Get Itinerary **/
        ItineraryRepresentation getResult = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
                accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                get(ItineraryRepresentation.class);
        
        /** Book the itinerary **/
        CreditCardWrapper cards = new CreditCardWrapper();
        cards.setLameCard(getLameCard());
        cards.setNiceCard(getNiceCard());
        
        String bookingPath = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/book";
        ItineraryRepresentation bookResult = client.resource(bookingPath).
            type(MEDIATYPE_ITINERARYPROCESS).
            put(ItineraryRepresentation.class, cards);
        
        /** Assertion Tests **/
        assertEquals("created", result.getStatus());
        assertTrue(flightResult1.getStatus().equals("flight added to itinerary"));
        assertTrue(hotelStatus1.getStatus().equals("hotel added to itinerary"));
        assertTrue(flightResult2.getStatus().equals("flight added to itinerary"));
        assertTrue(flightResult3.getStatus().equals("flight added to itinerary"));
        assertTrue(hotelStatus2.getStatus().equals("hotel added to itinerary"));
        assertEquals("open", getResult.getItinerary().getStatus());



    }
    
    @Test
    public void testP2(){
        
        Object[] cards = {getLameCard(), getNiceCard()};
        
        String cid = "4";
        String iid = "henning";
        
        StatusRepresentation result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
            accept(MEDIATYPE_ITINERARYPROCESS).
            type(MEDIATYPE_ITINERARYPROCESS).
            put(StatusRepresentation.class);

        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/cancel";
        StatusRepresentation cancelResult = client.resource(path).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);
        
        assertEquals("cancelled", cancelResult.getStatus(),cards);
    
    }
    
    @Test
    public void testB(){
        /** Confirm -> First Booking Success -> Second Booking Fail **/
        Object[] cards = {getLameCard(), getNiceCard()};
        
        String cid = "4";
        String iid = "henning";
        
        /** Create Itinerary **/
        StatusRepresentation result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
            accept(MEDIATYPE_ITINERARYPROCESS).
            type(MEDIATYPE_ITINERARYPROCESS).
            put(StatusRepresentation.class);
        
        /** Get Itinerary **/
        ItineraryRepresentation getResult = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
                accept(MEDIATYPE_ITINERARYPROCESS).
                type(MEDIATYPE_ITINERARYPROCESS).
                get(ItineraryRepresentation.class);
        
        /** Cancel **/
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/cancel";
        StatusRepresentation cancelResult = client.resource(path).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);
                
        assertEquals("open", getResult.getItinerary().getStatus(),cards);
        assertEquals("cancelled", cancelResult.getStatus(),cards);
    
    }
    
    @Test
    public void testC1(){
        
        /** Confirm -> Cancel **/
        Object[] cards = {getLameCard(), getNiceCard()};
        
        String cid = "4";
        String iid = "henning";
        
        StatusRepresentation result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
            accept(MEDIATYPE_ITINERARYPROCESS).
            type(MEDIATYPE_ITINERARYPROCESS).
            put(StatusRepresentation.class);

        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/cancel";
        StatusRepresentation cancelResult = client.resource(path).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);
        
        assertEquals("cancelled", cancelResult.getStatus(),cards);
    
    }
    
    @Test
    public void testC2(){
        /** Confirm -> Cancel -> Fail **/
        Object[] cards = {getLameCard(), getNiceCard()};
        
        String cid = "4";
        String iid = "henning";
        
        StatusRepresentation result = client.resource(itineraries.getURI().toString() + "/" + cid + "/"+ iid).
            accept(MEDIATYPE_ITINERARYPROCESS).
            type(MEDIATYPE_ITINERARYPROCESS).
            put(StatusRepresentation.class);

        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/cancel";
        StatusRepresentation cancelResult = client.resource(path).
                type(MEDIATYPE_ITINERARYPROCESS).
                put(StatusRepresentation.class);
        
        assertEquals("cancelled", cancelResult.getStatus(),cards);
    
    }
    
    /**************************/
    /**    SUPPORT METHODS   **/
    /**************************/
    
    /** Create Flights Query **/
    private FlightsQuery createFlightsQuery(XMLGregorianCalendar date, String fromName, String toName){
        String fromCode = airportToCodes.get(fromName);
        String toCode = airportToCodes.get(toName);
        AirportType fromAirport = new AirportType();
        fromAirport.setName(fromName);
        fromAirport.setCode(fromCode);
        
        AirportType toAirport = new AirportType();
        toAirport.setName(toName);
        toAirport.setCode(toCode);
        
        FlightPathType path = new FlightPathType();
        path.setFrom(fromAirport);
        path.setTo(toAirport);
        
        FlightsQuery newQuery = new FlightsQuery();
        newQuery.setDate(date);
        newQuery.setPath(path);
        
        return newQuery;
    }
    
    /**  Get Flights Call **/
     private FlightRepresentation getFlightsCall(FlightsQuery query, String cus, String it) throws DatatypeConfigurationException {
        String cid = cus;
        String iid = it;
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/flights";
        FlightRepresentation flightResult = client.resource(path).type(MEDIATYPE_ITINERARYPROCESS)
                .put(FlightRepresentation.class, query);
        return flightResult;
    }
     
     /** Get Hotels Call **/
    private HotelRepresentation getHotelsCall(HotelQuery request, String cus, String it) {
        String cid = cus;
        String iid = it;
        String path = itineraries.getURI().toString() + "/" + cid + "/" + iid + "/hotels";
        HotelRepresentation result = client.resource(path).type(MEDIATYPE_ITINERARYPROCESS).
                put(HotelRepresentation.class, request);
        return result;
    }
    
    /** Get Lame Duck Credit Card **/
    private CreditCardInfoType getLameCard(){
        dk.dtu.ws.lameduck.CreditCardInfoType lameCard = new CreditCardInfoType();
        dk.dtu.ws.lameduck.ExpirationDateType exp = new ExpirationDateType();
        exp.setMonth(5);
        exp.setYear(9);
        lameCard.setExpirationDate(exp);
        lameCard.setName("Anne Strandberg");
        lameCard.setNumber("5040881");
        return lameCard;
    }
    
    /** Get Nice View Credit Card **/
    private dk.dtu.ws.niceview.CreditCardInfoType getNiceCard(){
        dk.dtu.ws.niceview.CreditCardInfoType niceCard = new dk.dtu.ws.niceview.CreditCardInfoType();
        dk.dtu.ws.niceview.ExpirationDateType exp = new dk.dtu.ws.niceview.ExpirationDateType();
        exp.setMonth(5);
        exp.setYear(9);
        niceCard.setExpirationDate(exp);
        niceCard.setName("Anne Strandberg");
        niceCard.setNumber("5040881");
        return niceCard;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws.travelgood.test;

import dk.dtu.imm.fastmoney.types.CreditCardInfoType;
import dk.dtu.imm.fastmoney.types.ExpirationDateType;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import travelgood.ws.dtu.dk.*;
import types.lameduck.ws.dtu.dk.*;
import types.niceview.ws.dtu.dk.*;

/**
 *
 * @author jesper
 */
public class TravelGoodTest {
    private long itineraryId;
    
    public TravelGoodTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        //Get a timestamp for this moment, and use it for correlation
        itineraryId = new Date().getTime();
        
        createItinerary(itineraryId);
    }
    
    @After
    public void tearDown() {
    }
    
    
    @Test
    public void testP1() throws DatatypeConfigurationException{
        Flights flightList = getFlights(createGetFlightsRequest(3));
        FlightInfoType flight1 = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight1));
        
        HotelsType hotelList = getHotels(createGetHotelsRequest(1));
        HotelType hotel1 = hotelList.getHotel().get(0);
        addHotel(createAddHotelRequest(hotel1));
        
        flightList = getFlights(createGetFlightsRequest(1));
        FlightInfoType flight2 = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight2));
        
        flightList = getFlights(createGetFlightsRequest(4));
        FlightInfoType flight3 = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight3));
        
        hotelList = getHotels(createGetHotelsRequest(1));
        HotelType hotel2 = hotelList.getHotel().get(0);
        addHotel(createAddHotelRequest(hotel2));
        
        List<ItineraryItem> itinerary = getItinerary(itineraryId).getItem();
        assertEquals(5, itinerary.size());
        
        assertEquals(flight1.getBookingNumber(), itinerary.get(0).getFlight().getBookingNumber());
        assertEquals(hotel1.getBookingNumber(), itinerary.get(1).getHotel().getBookingNumber());
        assertEquals(flight2.getBookingNumber(), itinerary.get(2).getFlight().getBookingNumber());
        assertEquals(flight3.getBookingNumber(), itinerary.get(3).getFlight().getBookingNumber());
        assertEquals(hotel2.getBookingNumber(), itinerary.get(4).getHotel().getBookingNumber());
        
        for (ItineraryItem item : itinerary) {
            assertEquals("unconfirmed", item.getStatus());
        }
        
        cancelPlanning(itineraryId);
    }
    
    @Test
    public void testP2() {
        Flights flightList = getFlights(createGetFlightsRequest(6));
        FlightInfoType flight = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight));
        
        cancelPlanning(itineraryId);
    }
    
    @Test
    public void testB() throws DatatypeConfigurationException{        
        HotelsType hotelList = getHotels(createGetHotelsRequest(7));
        HotelType hotel = hotelList.getHotel().get(0);
        addHotel(createAddHotelRequest(hotel));
        
        Flights flightList = getFlights(createGetFlightsRequest(5));
        FlightInfoType flight = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight));
        
        flightList = getFlights(createGetFlightsRequest(8));
        flight = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight));
        
        BookItineraryRequest bookItineraryRequest = new BookItineraryRequest();
        bookItineraryRequest.setCreditCard(createWeakCreditCard());
        bookItineraryRequest.setItineraryId(itineraryId);
        
        boolean result = bookItinerary(bookItineraryRequest);
        assertFalse(result);
        List<ItineraryItem> itinerary = getItinerary(itineraryId).getItem();
        
        assertEquals("cancelled", itinerary.get(0).getStatus());
    }
    
    @Test
    public void testC1() throws DatatypeConfigurationException{
        HotelsType hotelList = getHotels(createGetHotelsRequest(4));
        HotelType hotel = hotelList.getHotel().get(0);
        addHotel(createAddHotelRequest(hotel));
        
        Flights flightList = getFlights(createGetFlightsRequest(11));
        FlightInfoType flight = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight));
        
        flightList = getFlights(createGetFlightsRequest(12));
        flight = flightList.getFlightInfo().get(0);
        addFlight(createAddFlightRequest(flight));
        
        BookItineraryRequest bookItineraryRequest = new BookItineraryRequest();
        bookItineraryRequest.setCreditCard(createStrongCreditCard());
        bookItineraryRequest.setItineraryId(itineraryId);
        
        boolean result = bookItinerary(bookItineraryRequest);
        //assertTrue(result);
        List<ItineraryItem> itinerary = getItinerary(itineraryId).getItem();
        
        for (ItineraryItem item : itinerary) {
            assertEquals("confirmed", item.getStatus());
        }
        
        cancelTravel(itineraryId);
        
        itinerary = getItinerary(itineraryId).getItem();
        
        for (ItineraryItem item : itinerary) {
            assertEquals("cancelled", item.getStatus());
        }
    }
    
    @Test
    public void testC2() {
        
        //Not implemented
        
        cancelPlanning(itineraryId);
    }
    
    private GetFlightsRequest createGetFlightsRequest (int flightId) {
        //Set up flight query
        FlightsQuery flightsQuery = createFlightsQuery(flightId);
        
        //Make getFlights request
        GetFlightsRequest getFlightsRequest = new GetFlightsRequest();
        getFlightsRequest.setItineraryId(itineraryId);
        getFlightsRequest.setFlightsQuery(flightsQuery);
        
        return getFlightsRequest;
    }
    
    private AddFlightRequest createAddFlightRequest (FlightInfoType flight) {
        AddFlightRequest addFlightRequest = new AddFlightRequest();
        addFlightRequest.setItineraryId(itineraryId);
        addFlightRequest.setFlight(flight);
        
        return addFlightRequest;
    }
    
    private FlightsQuery createFlightsQuery (int flightId) {
        FlightsQuery flightsQuery = new FlightsQuery();
        long baseTime = (flightId >= 10) ? 1420224162000L : 1416124162000L;
        flightsQuery.setDate(dateToXMLCal(baseTime + flightId*100000000));
        AirportType fromAirport = new AirportType();
        fromAirport.setCode("CPH");
        fromAirport.setName("Copenhagen Airport");
        AirportType toAirport = new AirportType();
        toAirport.setCode("ATH");
        toAirport.setName("Athens International Airport");
        FlightPathType path = new FlightPathType();
        path.setFrom(fromAirport);
        path.setTo(toAirport);
        flightsQuery.setPath(path);
        
        return flightsQuery;
    }
    
    private GetHotelsRequest createGetHotelsRequest (int hotelId) throws DatatypeConfigurationException {
        HotelQuery hotelQuery = createHotelQuery(hotelId);
        GetHotelsRequest getHotelsRequest = new GetHotelsRequest();
        getHotelsRequest.setItineraryId(itineraryId);
        getHotelsRequest.setHotelQuery(hotelQuery);
        
        return getHotelsRequest;
    }
    
    private AddHotelRequest createAddHotelRequest (HotelType hotel) {
        AddHotelRequest addHotelRequest = new AddHotelRequest();
        addHotelRequest.setItineraryId(itineraryId);
        addHotelRequest.setHotel(hotel);
        
        return addHotelRequest;
    }
    
    private HotelQuery createHotelQuery (int hotelId) throws DatatypeConfigurationException {
        DatatypeFactory df = DatatypeFactory.newInstance();
        HotelQuery query = new HotelQuery();
        query.setArrival(df.newXMLGregorianCalendar(hotelId % 3 == 1 ? "2014-12-15" : "2015-01-20"));
        query.setDeparture(df.newXMLGregorianCalendar(hotelId % 3 == 1 ? "2014-12-22" : "2015-01-27"));
        query.setCity(hotelId % 2 == 0 ? "Copenhagen" : "Aalborg");
        return query;
    }
    
    private CreditCardInfoType createWeakCreditCard() {
        CreditCardInfoType creditCard = new CreditCardInfoType();
        ExpirationDateType expirationDate = new ExpirationDateType();
        creditCard.setName("Bech Camilla");
        creditCard.setNumber("50408822");
        expirationDate.setMonth(7);
        expirationDate.setYear(9);
        creditCard.setExpirationDate(expirationDate);
        
        return creditCard;
    }
    
    private CreditCardInfoType createStrongCreditCard() {
        CreditCardInfoType creditCard = new CreditCardInfoType();
        ExpirationDateType expirationDate = new ExpirationDateType();
        creditCard.setName("Tick Joachim");
        creditCard.setNumber("50408824");
        expirationDate.setMonth(2);
        expirationDate.setYear(11);
        creditCard.setExpirationDate(expirationDate);
        
        return creditCard;
    }
    
    private static XMLGregorianCalendar dateToXMLCal(long time) {
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(time);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return xmlCal;
    }

    private static void createItinerary(long createItineraryRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        port.createItinerary(createItineraryRequest);
    }

    private static Flights getFlights(travelgood.ws.dtu.dk.GetFlightsRequest getFlightsRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.getFlights(getFlightsRequest);
    }

    private static boolean addFlight(travelgood.ws.dtu.dk.AddFlightRequest addFlightRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.addFlight(addFlightRequest);
    }

    private static HotelsType getHotels(travelgood.ws.dtu.dk.GetHotelsRequest getHotelsRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.getHotels(getHotelsRequest);
    }

    private static boolean addHotel(travelgood.ws.dtu.dk.AddHotelRequest addHotelRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.addHotel(addHotelRequest);
    }

    private static ItineraryType getItinerary(long getItineraryRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.getItinerary(getItineraryRequest);
    }

    private static void cancelPlanning(long cancelPlanningRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        port.cancelPlanning(cancelPlanningRequest);
    }

    private static boolean cancelTravel(long cancelTravelRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.cancelTravel(cancelTravelRequest);
    }

    private static boolean bookItinerary(travelgood.ws.dtu.dk.BookItineraryRequest bookItineraryRequest) {
        travelgood.ws.dtu.dk.TravelGoodService service = new travelgood.ws.dtu.dk.TravelGoodService();
        travelgood.ws.dtu.dk.TravelGoodPortType port = service.getTravelGoodPortTypeBindingPort();
        return port.bookItinerary(bookItineraryRequest);
    }
}
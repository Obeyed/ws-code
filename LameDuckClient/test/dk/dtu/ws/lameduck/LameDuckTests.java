/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws.lameduck;

import dk.dtu.imm.fastmoney.types.CreditCardInfoType;
import dk.dtu.imm.fastmoney.types.ExpirationDateType;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import lameduck.ws.dtu.dk.BookFlightFault;
import types.lameduck.ws.dtu.dk.AirportType;
import types.lameduck.ws.dtu.dk.BookFlightRequest;
import types.lameduck.ws.dtu.dk.CancelFlightRequest;
import types.lameduck.ws.dtu.dk.FlightInfoType;
import types.lameduck.ws.dtu.dk.FlightPathType;
import types.lameduck.ws.dtu.dk.FlightType;
import types.lameduck.ws.dtu.dk.Flights;
import types.lameduck.ws.dtu.dk.FlightsQuery;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author joshweinstein
 */
public class LameDuckTests {
    
    public LameDuckTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getFlightsTest() {
        FlightsQuery query = new FlightsQuery();
        XMLGregorianCalendar departure = dateToXMLCal(1416124162000L + 100000000);
        query.setDate(departure);
        
        FlightPathType path = new FlightPathType();
        AirportType from = createAirportType("CPH", "Copenhagen Airport");
        path.setFrom(from);
        AirportType to = createAirportType("ATH", "Athens International Aiport");
        path.setTo(to);
        query.setPath(path);
        
        Flights queriedFlights = getFlights(query);
        assertTrue(queriedFlights.getFlightInfo().size() > 0);
        FlightInfoType returnedFlight = queriedFlights.getFlightInfo().get(0);
        assertEquals("DKK", returnedFlight.getPrice().getCurrency());
        assertTrue(returnedFlight.getPrice().getAmount() > 0);
    }

    @Test(expected=Exception.class)
    public void bookFlightsBadCCTest() throws BookFlightFault {
        BookFlightRequest request = new BookFlightRequest();
        request.setBookingNumber("1");
        CreditCardInfoType cc = new CreditCardInfoType();
        cc.setName("Foo Bar");
        ExpirationDateType expiration = new ExpirationDateType();
        expiration.setMonth(00);
        expiration.setYear(0000);
        cc.setExpirationDate(expiration);
        cc.setNumber("00000000");
        request.setCreditCard(cc);
        
        bookFlight(request);
    }
    
    private static AirportType createAirportType(String code, String name) {
        AirportType airport = new AirportType();
        airport.setCode(code);
        airport.setName(name);
        return airport;
    }
    
    private static XMLGregorianCalendar dateToXMLCal(long time) {
        GregorianCalendar cal = new GregorianCalendar();
        Date date = new Date(time);
        cal.setTime(date);
        XMLGregorianCalendar xmlCal = null;
        try {
            xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        return xmlCal;
    }

    private static boolean bookFlight(types.lameduck.ws.dtu.dk.BookFlightRequest bookFlightRequest) throws BookFlightFault {
        lameduck.ws.dtu.dk.LameDuckService service = new lameduck.ws.dtu.dk.LameDuckService();
        lameduck.ws.dtu.dk.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        return port.bookFlight(bookFlightRequest);
    }

    private static void cancelFlight(types.lameduck.ws.dtu.dk.CancelFlightRequest cancelFlightRequest) {
        lameduck.ws.dtu.dk.LameDuckService service = new lameduck.ws.dtu.dk.LameDuckService();
        lameduck.ws.dtu.dk.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        port.cancelFlight(cancelFlightRequest);
    }

    private static Flights getFlights(types.lameduck.ws.dtu.dk.FlightsQuery getFlightsQuery) {
        lameduck.ws.dtu.dk.LameDuckService service = new lameduck.ws.dtu.dk.LameDuckService();
        lameduck.ws.dtu.dk.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        return port.getFlights(getFlightsQuery);
    }

}
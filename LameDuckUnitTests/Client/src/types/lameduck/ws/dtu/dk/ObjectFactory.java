
package types.lameduck.ws.dtu.dk;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the types.lameduck.ws.dtu.dk package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BookFlightResponse_QNAME = new QName("dk.dtu.ws.lameduck.types", "bookFlightResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: types.lameduck.ws.dtu.dk
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BookFlightFault }
     *
     */
    public BookFlightFault createBookFlightFault() {
        return new BookFlightFault();
    }

    /**
     * Create an instance of {@link CancelFlightRequest }
     *
     */
    public CancelFlightRequest createCancelFlightRequest() {
        return new CancelFlightRequest();
    }

    /**
     * Create an instance of {@link MoneyType }
     *
     */
    public MoneyType createMoneyType() {
        return new MoneyType();
    }

    /**
     * Create an instance of {@link Flights }
     *
     */
    public Flights createFlights() {
        return new Flights();
    }

    /**
     * Create an instance of {@link FlightInfoType }
     *
     */
    public FlightInfoType createFlightInfoType() {
        return new FlightInfoType();
    }

    /**
     * Create an instance of {@link BookFlightRequest }
     *
     */
    public BookFlightRequest createBookFlightRequest() {
        return new BookFlightRequest();
    }

    /**
     * Create an instance of {@link FlightsQuery }
     *
     */
    public FlightsQuery createFlightsQuery() {
        return new FlightsQuery();
    }

    /**
     * Create an instance of {@link FlightPathType }
     *
     */
    public FlightPathType createFlightPathType() {
        return new FlightPathType();
    }

    /**
     * Create an instance of {@link CarrierType }
     *
     */
    public CarrierType createCarrierType() {
        return new CarrierType();
    }

    /**
     * Create an instance of {@link FlightType }
     *
     */
    public FlightType createFlightType() {
        return new FlightType();
    }

    /**
     * Create an instance of {@link AirportType }
     *
     */
    public AirportType createAirportType() {
        return new AirportType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "dk.dtu.ws.lameduck.types", name = "bookFlightResponse")
    public JAXBElement<Boolean> createBookFlightResponse(Boolean value) {
        return new JAXBElement<Boolean>(_BookFlightResponse_QNAME, Boolean.class, null, value);
    }

}

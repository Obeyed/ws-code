package dk.dtu;

import java.util.Date;

public class CreditCard {
    private final String cardHolder;
    private final byte[] expiryDate = new byte[4];
    private final int securityCode; // Code on back of card.
    private final byte[] number = new byte[16]; // Credit Card Number
    
    public CreditCard(final String cardHolder, final byte[] expiryDate, final int securityCode, final byte[] number) {
        super();
        this.cardHolder = cardHolder;
        this.securityCode = securityCode;
        // TODO implement the char assigns.
    }


    public String getCardHolder() {
        return this.cardHolder;
    }

    public int getSecurityCode() {
        return this.securityCode;
    }

    public byte[] getExpiryDate() {
        return this.expiryDate;
    }
    
    public byte[] getNumber() {
        return this.number;
    }
}

package dk.dtu.airline;

import dk.dtu.CreditCard;

import java.util.Date;
import java.util.List;

/**
 * Interface to Airline Reservation service, as defined by Hubert.
 */
public interface AirlineReservation {
    
    public List<FlightInfo> getFlights(final FlightPath path, final Date date);
    
    public boolean bookFlight(final String bookingNumber, CreditCard creditCard);
    
    public void cancelFlight(final String bookingNumber, double price, CreditCard creditCard);
    
}

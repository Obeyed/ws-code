package dk.dtu.airline;

import dk.dtu.Money;

/**
 * Can also be understood as a seat on a flight.
 * A single seat on a flight, as booked by a customer.
 * 
 * @author P�tur Ingi Egilsson
 */
public class FlightInfo {

    private final String bookingNumber;
    private final AirlineReservationService reservedWith;
    private final Flight flight;
    private final Money price;
    
    /**
     *
     * @param bookingNumber The booking number is unique with the air reservation service.
     * @param reservedWith The booking service used to reserve the seat.
     * @param flight The flight to which this booking corresponds.
     * @param price The price paid by the customer.
     * 
     * @author P�tur Ingi Egilsson
     */
    public FlightInfo(final String bookingNumber,
                      final AirlineReservationService reservedWith,
                      final Flight flight,
                      final Money price) {
        super();
        // TODO ensure no parameter is null.
        this.bookingNumber = bookingNumber;
        this.reservedWith = reservedWith;
        this.flight = flight;
        this.price = price;
    }
    
    public String getBookingNumber() {
        return bookingNumber;
    }

    public AirlineReservationService getReservedWith() {
        return reservedWith;
    }

    public Flight getFlight() {
        return flight;
    }

    public Money getPrice() {
        return price;
    }
}

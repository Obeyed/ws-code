package dk.dtu.airline;

import java.util.Date;

/**
 * Flight transfers passengers between two locations.
 * An airplane.
 *
 * @author P�tur Ingi Egilsson
 */
public class Flight {
    
    private final Carrier operator;
    private final Date liftoffTime;
    private final Date landingTime;
    private final FlightPath flightPath;
    
    /**
     * A flight is a single travel by airplane between two destinations.
     * 
     * @param operator Who is responsible for the flight.
     * @param departureAirport The departure airport.
     * @param destinationAirport The destination airport.
     * @param liftoffTime The time and date of which the airplane lifts off the ground at its departure airport.
     * @param landingTime The time and date of which the airplane lands at its destination airport.
     * 
     * @author P�tur Ingi Egilsson
     */
    public Flight(Carrier operator, Airport departureAirport, Airport destinationAirport, Date liftoffTime, Date landingTime) {
        // TODO confirm no parameter is null.
        super();
        this.operator = operator;
        this.liftoffTime = liftoffTime;
        this.landingTime = landingTime;
        this.flightPath = new FlightPath(departureAirport, destinationAirport);
    }
    
    public FlightPath getFlightPath() {
        return this.flightPath;
    }
    
    public String toString() {
        return "Carrier: " + this.operator + "\n"
            + "Liftoff from " + this.flightPath.departure.toString() + " at " + this.liftoffTime.toString() + "\n"
            + "Landing at " + this.flightPath.destination.toString() + " at " + this.landingTime.toString(); 
    }
}

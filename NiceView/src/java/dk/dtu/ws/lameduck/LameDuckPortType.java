
package dk.dtu.ws.lameduck;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.5-b05 
 * Generated source version: 2.1
 * 
 */
@WebService(name = "LameDuckPortType", targetNamespace = "dk.dtu.ws.lameduck")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface LameDuckPortType {


    /**
     * 
     * @param getFlightsQuery
     * @return
     *     returns dk.dtu.ws.lameduck.Flights
     */
    @WebMethod(operationName = "GetFlights")
    @WebResult(name = "flights", targetNamespace = "dk.dtu.ws.lameduck.types", partName = "getFlightsResponse")
    public Flights getFlights(
        @WebParam(name = "flightsQuery", targetNamespace = "dk.dtu.ws.lameduck.types", partName = "getFlightsQuery")
        FlightsQuery getFlightsQuery);

    /**
     * 
     * @param bookFlightRequest
     * @return
     *     returns boolean
     * @throws BookFlightFault_Exception
     */
    @WebMethod(operationName = "BookFlight")
    @WebResult(name = "bookFlightResponse", targetNamespace = "dk.dtu.ws.lameduck.types", partName = "bookFlightResponse")
    public boolean bookFlight(
        @WebParam(name = "bookFlightRequest", targetNamespace = "dk.dtu.ws.lameduck.types", partName = "bookFlightRequest")
        BookFlightRequest bookFlightRequest)
        throws BookFlightFault_Exception
    ;

    /**
     * 
     * @param cancelFlightRequest
     */
    @WebMethod(operationName = "CancelFlight")
    @Oneway
    public void cancelFlight(
        @WebParam(name = "cancelFlightRequest", targetNamespace = "dk.dtu.ws.lameduck.types", partName = "cancelFlightRequest")
        CancelFlightRequest cancelFlightRequest);

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws;

import dk.dtu.imm.fastmoney.BankService;
import dk.dtu.imm.fastmoney.CreditCardFaultMessage;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import niceview.ws.dtu.dk.BookHotelFaultType;
import types.niceview.ws.dtu.dk.AddressType;
import types.niceview.ws.dtu.dk.FaultType;
import types.niceview.ws.dtu.dk.HotelType;
import types.niceview.ws.dtu.dk.HotelsType;

/**
 *
 * @author obeid
 */
@WebService(serviceName = "NiceViewWSDLService", portName = "NiceViewWSDLPortTypeBindingPort", endpointInterface = "niceview.ws.dtu.dk.NiceViewWSDLPortType", targetNamespace = "http://dk.dtu.ws.niceview", wsdlLocation = "WEB-INF/wsdl/NiceViewService/NiceViewWSDL.wsdl")
public class NiceViewService {
//    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/fastmoney.imm.dtu.dk_8080/BankService.wsdl")
    private BankService service = new BankService();
    /**
     * List of hotels
     */
    private List<HotelType> hotels = generateHotels();
    /**
     * List of active bookings
     */
    private List<String> bookings = new ArrayList<String>();
    
    /**
     * Generates hotels on startup.
     * @return List of hotels.
     */
    private List<HotelType> generateHotels() {
        HotelsType h = new HotelsType();
        List <HotelType> list = h.getHotel();
        DatatypeFactory df = null;
        try{
            df = DatatypeFactory.newInstance();
        }
        catch(DatatypeConfigurationException dce){
            System.out.println("Exception thrown in generateHotels() by DataFactory!");
            System.exit(2);
        }
        
        for(int i = 0; i < 10; i++){
            AddressType address = new AddressType();
            address.setCity(i % 2 == 0 ? "Copenhagen" : "Aalborg");
            address.setCountry("Denmark");
            address.setPostalcode(String.valueOf(i));
            address.setStreet("Street" + i);
            HotelType hotel = new HotelType();
            hotel.setAddress(address);
            hotel.setBookingNumber(String.valueOf(i));
            hotel.setCreditCardGuarantee(i % 3 == 0 ? true : false);
            hotel.setHotelReservationService("NiceView");
            hotel.setName("HotelName" + i);
            hotel.setPrice(199);
            hotel.setArrival(df.newXMLGregorianCalendar((i % 3 == 1 ? "2014-12-15" : "2015-01-20") + "T00:00:00"));
            hotel.setDeparture(df.newXMLGregorianCalendar((i % 3 == 1 ? "2014-12-22" : "2015-01-27") + "T00:00:00"));
            list.add(hotel);
        }
        
        return list;
    }
    
    /**
     * Get list of hotels.
     * @param request Request contains arrival & departure dates and city.
     * @return List of hotels. Can be empty.
     */
    public types.niceview.ws.dtu.dk.HotelsType getHotels(types.niceview.ws.dtu.dk.HotelQuery request) {
        HotelsType hotelList = new HotelsType();
        String requestCity = request.getCity();
        XMLGregorianCalendar requestArrival = request.getArrival();
        XMLGregorianCalendar requestDeparture = request.getDeparture();
        for(HotelType h : hotels){
            if(h.getAddress().getCity().equalsIgnoreCase(requestCity)
                    && DateHelper.compareDates(h.getArrival(), requestArrival)
                    && DateHelper.compareDates(h.getDeparture(), requestDeparture))
                hotelList.getHotel().add(h);
        }
        
        return hotelList;
    }
    
    /**
     * Book a hotel.
     * @param request Request contains booking number and credit card information.
     * @return True if succeeds.
     * @throws BookHotelFaultType If booking number cannot be found, 
     *   or credit card information cannot be validated (only if credit card guarantee is required).
     */
    public boolean bookHotel(niceview.ws.dtu.dk.BookHotelRequest request) throws BookHotelFaultType {
        HotelType hotel = null;
        for(HotelType h : hotels){
            if(h.getBookingNumber().equals(request.getBookingNumber())){
                hotel = h;
                System.out.println("Found hotel with booking number: " + request.getBookingNumber());
                break;
            }
        }
        
        if (hotel == null) {
            FaultType faultInfo = new FaultType();
            faultInfo.setFaultString("Error while booking unknown hotel.");
            throw new BookHotelFaultType("Hotel is null.", faultInfo);
        }
        
        if(hotel.isCreditCardGuarantee()){
            try {
               validateCreditCard(16, request.getCreditCardInfo(), hotel.getPrice());
            } catch (CreditCardFaultMessage ex) {
                FaultType faultInfo = new FaultType();
                faultInfo.setFaultString("Unable to book hotel with credit card information: " + request.getCreditCardInfo().getNumber());
                throw new BookHotelFaultType(ex.getFaultInfo().getMessage(), faultInfo);
            } 
        }
        
        bookings.add(request.getBookingNumber()); // Add the booking number to current bookings
        return true;
    }

    public void cancelHotel(types.niceview.ws.dtu.dk.CancelHotelRequest request) throws Exception {
         Boolean cancelled = false;
        for(int i = 0; i < bookings.size(); i++)
            if(bookings.get(i).equals(request.getBookingNumber())){
                bookings.remove(i);
                cancelled = true;
            }
        if(!cancelled){
            throw new Exception("Unable to cancel booking with number: " + request.getBookingNumber());
        }
    }

    private boolean validateCreditCard(int group, dk.dtu.imm.fastmoney.types.CreditCardInfoType creditCardInfo, int amount) throws CreditCardFaultMessage {
        dk.dtu.imm.fastmoney.BankPortType port = service.getBankPort();
        return port.validateCreditCard(group, creditCardInfo, amount);
    }
    
}


package travelgood.ws.itineraryprocess.resource;

import dk.dtu.ws.lameduck.CancelFlightRequest;
import dk.dtu.ws.lameduck.CreditCardInfoType;
import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.niceview.CancelHotelRequest;
import dk.dtu.ws.niceview.HotelType;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import travelgood.ws.entities.CreditCardWrapper;
import travelgood.ws.entities.Itinerary;
import travelgood.ws.itineraryprocess.representation.StatusRepresentation;

@Path("itineraries/{cid}/{iid}/cancel")
public class ItineraryCancelResource {
   
    @PUT
    @Consumes(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    @Produces(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    public Response cancelItinerary(@PathParam("cid") String cid, @PathParam("iid") String iid, CreditCardWrapper cards) {
         
        CreditCardInfoType lameCard = cards.getLameCard(); 
        dk.dtu.ws.niceview.CreditCardInfoType niceCard = cards.getNiceCard();
        
        String key = ItineraryResource.createKey(cid, iid);
        Itinerary itinerary = ItineraryResource.itineraries.get(key);
        StatusRepresentation response = new StatusRepresentation();

        if (itinerary == null) {
            return Response.
                    status(Response.Status.NOT_FOUND).
                    entity("Itinerary not found").
                    build();
        }
        
        // If already cancelled 
        if(itinerary.getStatus().equals(ItineraryResource.STATUS_CANCELLED)){
            response.setStatus(itinerary.getStatus());
            ItineraryResource.addSelfLink(cid,iid,response);
            return Response.ok(response).build();
        }
        
        // for unbooked itineraries
        if(!itinerary.getStatus().equals(ItineraryResource.STATUS_BOOKED)){
            itinerary.setStatus(ItineraryResource.STATUS_CANCELLED);
            response.setStatus(itinerary.getStatus());
            ItineraryResource.addSelfLink(cid,iid,response);
            return Response.ok(response).build();
        }
        
        // Getting Time in millis
        GregorianCalendar date = new GregorianCalendar();
        int time = (int) date.getTimeInMillis();
        
        if(itinerary.getIsCancelAllowed(time)){
            unBook(itinerary.getFlights(), itinerary.getHotels(),lameCard, niceCard);
        }else{
            return Response.
                    status(Response.Status.BAD_REQUEST).
                    entity("Too late to cancel").
                    build();
        }
        
        itinerary.setStatus(ItineraryResource.STATUS_CANCELLED);
        response.setStatus(itinerary.getStatus());
        ItineraryResource.itineraries.put(key, itinerary);
        ItineraryResource.addSelfLink(cid,iid,response);

        // todo: do refund shizzle
        return Response.ok(response).build();
    }  
    
     private static void unBook(ArrayList<FlightInfoType> bookedFlights, ArrayList<HotelType> bookedHotels, CreditCardInfoType lameCard, dk.dtu.ws.niceview.CreditCardInfoType niceCard){
        
        /** UnBooking Flights **/
        for(FlightInfoType flight : bookedFlights){
            CancelFlightRequest request = new CancelFlightRequest();
            request.setBookingNumber(flight.getBookingNumber());
            request.setCreditCard(lameCard);
            request.setPrice(flight.getPrice());
            cancelFlight(request);
        }
        
        /** UnBooking Hotels **/
        for(HotelType hotel : bookedHotels){
            CancelHotelRequest request = new CancelHotelRequest();
            request.setBookingNumber(hotel.getBookingNumber());
            cancelHotel(request);
        }
        
    }
    
    private static void cancelFlight(dk.dtu.ws.lameduck.CancelFlightRequest cancelFlightRequest) {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        port.cancelFlight(cancelFlightRequest);
    }
       
    private static void cancelHotel(dk.dtu.ws.niceview.CancelHotelRequest request) {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        port.cancelHotel(request);
    }
    
}

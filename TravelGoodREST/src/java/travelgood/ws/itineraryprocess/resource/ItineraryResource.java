package travelgood.ws.itineraryprocess.resource;

import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import travelgood.ws.itineraryprocess.representation.*;
import travelgood.ws.entities.Itinerary;

@Path("itineraries/{cid}/{iid}")
public class ItineraryResource {
    
    static final String STATUS_ITINERARY_CREATED = "created";
    static final String STATUS_CANCELLED = "cancelled";
    static final String STATUS_BOOKED = "booked";
    static final String STATUS_OPEN = "open";
    static final String STATUS_NO_FLIGHTS_FOUND = "no flights found";
    static final String STATUS_FLIGHT_ADDED = "flight added to itinerary";
    static final String STATUS_HOTEL_ADDED = "hotel added to itinerary";
    static final String STATUS_BAD_QUERY = "query not found";
    
    static final String RELATION_BASE = "http://itineraryprocess.ws/relations/";    
    static final String CANCEL_RELATION = RELATION_BASE + "cancel";
    static final String BOOK_RELATION = RELATION_BASE + "book";
    static final String SELF_RELATION = RELATION_BASE + "planning";
    static final String FLIGHTS_RELATION = RELATION_BASE + "flights";
    static final String ADD_FLIGHT_RELATION = RELATION_BASE + "addFlight";
    static final String ADD_HOTEL_RELATION = RELATION_BASE + "addHotel";
    static final String HOTELS_RELATION = RELATION_BASE + "hotels";
    
    static final String MEDIATYPE_ITINERARYPROCESS = "application/itineraryprocess+xml"; 
    static final String BASE_URI = "http://localhost:8080/TravelGoodREST/itineraries";
    
    
    public static Map<String, Itinerary> itineraries = new HashMap<String, Itinerary>();
    
    
    
    @PUT
    @Consumes(MEDIATYPE_ITINERARYPROCESS)
    @Produces(MEDIATYPE_ITINERARYPROCESS)
    public Response createItinerary(@PathParam("cid") String cid,
            @PathParam("iid") String iid) {

        StatusRepresentation statusRep = new StatusRepresentation();

        statusRep.setStatus(STATUS_ITINERARY_CREATED);
        String key = createKey(cid, iid);
        
        //TODO (if needed) check for same iid
        Itinerary newItinerary = new Itinerary();
        newItinerary.setId(iid);
        
        newItinerary.setStatus(STATUS_OPEN);
        itineraries.put(key, newItinerary);

        addSelfLink(cid, iid, statusRep);
        addCancelLink(cid, iid, statusRep);
        addBookLink(cid, iid, statusRep);
        addFlightsLink(cid, iid, statusRep);
        addHotelsLink(cid,iid,statusRep);

        return Response.ok(statusRep).build();
    }
    
    
    @GET
    @Produces(MEDIATYPE_ITINERARYPROCESS)
    public Response getItinerary(@PathParam("cid") String cid,@PathParam("iid") String iid) {

        String key = createKey(cid, iid);
        Itinerary itinerary = itineraries.get(key);
        if (itinerary == null) {
            return Response.status(Response.Status.NOT_FOUND).entity("Itinerary not found").build();
        }
        
        ItineraryRepresentation itineraryRep = new ItineraryRepresentation();
        itineraryRep.setItinerary(itinerary);
        
        // Always Self ref
        addSelfLink(cid, iid, itineraryRep);

        // Cancelled?
        if (!itinerary.getStatus().equals(STATUS_CANCELLED) || itinerary.getStatus().equals(STATUS_BOOKED)){
            addCancelLink(cid, iid, itineraryRep);
        }
        
        // Open ?
        if (itinerary.getStatus().equals(STATUS_OPEN)) {
            addBookLink(cid, iid, itineraryRep);
            addFlightsLink(cid, iid, itineraryRep);
            addHotelsLink(cid,iid,itineraryRep);
        }
        
        return Response.ok(itineraryRep).build();
    }
    
    
    
    static String createKey(String cid, String iid) {
        return String.format("%s,%s", cid, iid);
    }
    
    static void addCancelLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s/cancel", BASE_URI, cid, iid));
        link.setRel(CANCEL_RELATION);
        response.getLinks().add(link);
    }

    static void addBookLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s/book", BASE_URI, cid, iid));
        link.setRel(BOOK_RELATION);
        response.getLinks().add(link);
    }

    static void addFlightsLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s/flights", BASE_URI, cid, iid));
        link.setRel(FLIGHTS_RELATION);
        response.getLinks().add(link);
    }
    
    static void addAddFlightLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s/addflight", BASE_URI, cid, iid));
        link.setRel(ADD_FLIGHT_RELATION);
        response.getLinks().add(link);
    }
    
    static void addHotelsLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s/hotels", BASE_URI, cid, iid));
        link.setRel(HOTELS_RELATION);
        response.getLinks().add(link);
    }

    static void addAddHotelLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s/addHotel", BASE_URI, cid, iid));
        link.setRel(ADD_HOTEL_RELATION);
        response.getLinks().add(link);
    }  
    
    static void addSelfLink(String cid, String iid, Representation response) {
        Link link = new Link();
        link.setUri(String.format("%s/%s/%s", BASE_URI, cid, iid));
        link.setRel(SELF_RELATION);
        response.getLinks().add(link);
    }
        
}



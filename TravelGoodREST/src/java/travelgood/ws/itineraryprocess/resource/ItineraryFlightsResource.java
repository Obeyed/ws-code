package travelgood.ws.itineraryprocess.resource;

import dk.dtu.ws.lameduck.Flights;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import travelgood.ws.entities.Itinerary;
import travelgood.ws.itineraryprocess.representation.FlightRepresentation;
import travelgood.ws.itineraryprocess.representation.StatusRepresentation;

import dk.dtu.ws.lameduck.FlightsQuery;


@Path("itineraries/{cid}/{iid}/flights")
public class ItineraryFlightsResource {  
    
    @PUT
    @Consumes(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    @Produces(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    public Response getFlights(@PathParam("cid") String cid, @PathParam("iid") String iid, FlightsQuery flightQuery) { 
        
        if(flightQuery == null){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
          
        Flights result = getLameFlights(flightQuery);
        
        if(result.getFlightInfo().isEmpty()) {
            return Response.status(Response.Status.NO_CONTENT).build(); 
        }
        
        FlightRepresentation response = new FlightRepresentation();
        response.setFlights(result);
        
        ItineraryResource.addSelfLink(cid,iid,response);
        ItineraryResource.addAddFlightLink(cid,iid,response);
        return Response.ok(response).build();
    }  

    private static Flights getLameFlights(dk.dtu.ws.lameduck.FlightsQuery getFlightsQuery) {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        return port.getFlights(getFlightsQuery);
    }
}
    


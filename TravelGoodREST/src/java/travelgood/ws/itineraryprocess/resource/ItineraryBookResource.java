package travelgood.ws.itineraryprocess.resource;

import com.sun.xml.bind.v2.runtime.IllegalAnnotationException;
import com.sun.xml.bind.v2.runtime.IllegalAnnotationsException;
import dk.dtu.ws.lameduck.BookFlightFault_Exception;
import dk.dtu.ws.lameduck.BookFlightRequest;
import dk.dtu.ws.lameduck.CancelFlightRequest;
import dk.dtu.ws.lameduck.CreditCardFaultType;
import dk.dtu.ws.lameduck.CreditCardInfoType;
import dk.dtu.ws.lameduck.FlightInfoType;
import dk.dtu.ws.niceview.BookHotelFaultType;
import dk.dtu.ws.niceview.BookHotelRequest;
import dk.dtu.ws.niceview.CancelHotelRequest;
import dk.dtu.ws.niceview.HotelType;
import java.util.ArrayList;
import java.util.HashMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import travelgood.ws.entities.Itinerary;
import travelgood.ws.itineraryprocess.representation.StatusRepresentation;
import java.lang.Object;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.*;
import travelgood.ws.entities.CreditCardWrapper;

@Path("itineraries/{cid}/{iid}/book")
public class ItineraryBookResource {
    
    @PUT
    @Consumes(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    @Produces(ItineraryResource.MEDIATYPE_ITINERARYPROCESS)
    public Response bookItinerary(@PathParam("cid") String cid, @PathParam("iid") String iid, CreditCardWrapper creditCardWrapper) throws BookFlightFault_Exception, BookHotelFaultType {
        dk.dtu.ws.lameduck.CreditCardInfoType lameCard = creditCardWrapper.getLameCard(); 
        dk.dtu.ws.niceview.CreditCardInfoType niceCard = creditCardWrapper.getNiceCard();
        
        String key = ItineraryResource.createKey(cid, iid);
        Itinerary itinerary = ItineraryResource.itineraries.get(key);
        
        if (itinerary == null) {
            return Response.
                    status(Response.Status.NOT_FOUND).
                    entity("Itinerary not found").
                    build();
        }
        
        // Check for any flights or hotels

        ItineraryResource.itineraries.put(key, itinerary);
        
        /** For UnBooking **/
        ArrayList<FlightInfoType> bookedFlights = new ArrayList<FlightInfoType>();
        ArrayList<HotelType> bookedHotels = new ArrayList<HotelType>();
        
        /** Booking via lameduck **/ 
        for(FlightInfoType flight : itinerary.getFlights()){
            BookFlightRequest bookRequest = new BookFlightRequest();
            bookRequest.setBookingNumber(flight.getBookingNumber());
            bookRequest.setCreditCard(lameCard);
            bookedFlights.add(flight);
            boolean success = bookFlight(bookRequest);
                if(!success){
                    unBook(bookedFlights, bookedHotels, lameCard, niceCard);
                    return Response.status(Response.Status.CONFLICT). 
                            entity("Could not book"). 
                            build();
                }
        }
          
        /** Booking via niceview **/
        for(HotelType hotel : itinerary.getHotels()){
            BookHotelRequest bookRequest = new BookHotelRequest();
            bookRequest.setBookingNumber(hotel.getBookingNumber());
            bookRequest.setCreditCardInfo(niceCard);
            bookedHotels.add(hotel);
            boolean success = bookHotel(bookRequest);
            if(!success){
               unBook(bookedFlights, bookedHotels, lameCard, niceCard);
                return Response.status(Response.Status.CONFLICT). 
                        entity("Could not book"). 
                        build();
                }
        }
        
        StatusRepresentation response = new StatusRepresentation();
        ItineraryResource.addSelfLink(cid,iid,response);
        itinerary.setStatus(ItineraryResource.STATUS_BOOKED);
        response.setStatus(itinerary.getStatus());
        return Response.ok(response).build();
    }
    
    private static void unBook(ArrayList<FlightInfoType> bookedFlights, ArrayList<HotelType> bookedHotels, CreditCardInfoType lameCard, dk.dtu.ws.niceview.CreditCardInfoType niceCard){
        
        /** UnBooking Flights **/
        for(FlightInfoType flight : bookedFlights){
            CancelFlightRequest request = new CancelFlightRequest();
            request.setBookingNumber(flight.getBookingNumber());
            request.setCreditCard(lameCard);
            request.setPrice(flight.getPrice());
            cancelFlight(request);
        }
        
        /** UnBooking Hotels **/
        for(HotelType hotel : bookedHotels){
            CancelHotelRequest request = new CancelHotelRequest();
            request.setBookingNumber(hotel.getBookingNumber());
            cancelHotel(request);
        }
        
    }
    
    private static boolean bookFlight(dk.dtu.ws.lameduck.BookFlightRequest bookFlightRequest) throws BookFlightFault_Exception {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        return port.bookFlight(bookFlightRequest);
    }

    private static boolean bookHotel(dk.dtu.ws.niceview.BookHotelRequest request) throws BookHotelFaultType {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        return port.bookHotel(request);
    }

    private static void cancelFlight(dk.dtu.ws.lameduck.CancelFlightRequest cancelFlightRequest) {
        dk.dtu.ws.lameduck.LameDuckService service = new dk.dtu.ws.lameduck.LameDuckService();
        dk.dtu.ws.lameduck.LameDuckPortType port = service.getLameDuckPortTypeBindingPort();
        port.cancelFlight(cancelFlightRequest);
    }
       
    private static void cancelHotel(dk.dtu.ws.niceview.CancelHotelRequest request) {
        dk.dtu.ws.niceview.NiceViewWSDLService service = new dk.dtu.ws.niceview.NiceViewWSDLService();
        dk.dtu.ws.niceview.NiceViewWSDLPortType port = service.getNiceViewWSDLPortTypeBindingPort();
        port.cancelHotel(request);
    }
}

package travelgood.ws.itineraryprocess.representation;

import javax.xml.bind.annotation.XmlRootElement;
import travelgood.ws.entities.Itinerary;

@XmlRootElement()
public class ItineraryRepresentation extends Representation {
    
    private Itinerary itinerary;

    public Itinerary getItinerary() {
        return this.itinerary;
    }

    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }
}

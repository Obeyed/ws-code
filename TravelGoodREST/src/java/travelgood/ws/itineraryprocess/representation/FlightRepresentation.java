/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package travelgood.ws.itineraryprocess.representation;

import dk.dtu.ws.lameduck.Flights;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jcfinnerup
 */
@XmlRootElement()
public class FlightRepresentation extends Representation {
    
    private Flights flights;

    public Flights getFlights() {
        return this.flights;
    }

    public void setFlights(Flights flights) {
        this.flights = flights;
    }
}

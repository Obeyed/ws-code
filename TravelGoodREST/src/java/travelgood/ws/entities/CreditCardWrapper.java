/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package travelgood.ws.entities;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author joshweinstein
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cardsCreditCardWrapper", propOrder = {
    "lameCard",
    "niceCard"
})
@XmlRootElement(name = "CreditCardWrapper")
public class CreditCardWrapper {
    
    @XmlElement(namespace = "",required = true)
    protected dk.dtu.ws.lameduck.CreditCardInfoType lameCard;
    
    @XmlElement(namespace = "",required = true)
    protected dk.dtu.ws.niceview.CreditCardInfoType niceCard;
    
    
    public dk.dtu.ws.lameduck.CreditCardInfoType getLameCard() {
        return this.lameCard;
    }
    
    public dk.dtu.ws.niceview.CreditCardInfoType getNiceCard() {
        return this.niceCard;
    }
    
    public void setLameCard(dk.dtu.ws.lameduck.CreditCardInfoType lameCard) {
        this.lameCard = lameCard;
    }
    
    public void setNiceCard(dk.dtu.ws.niceview.CreditCardInfoType niceCard) {
        this.niceCard = niceCard;
    }
}

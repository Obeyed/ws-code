/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.dtu.ws.lameduck;

import types.lameduck.ws.dtu.dk.AirportType;
import types.lameduck.ws.dtu.dk.FlightPathType;

/**
 *
 * @author joshweinstein
 */
public class Comparator {
    public static boolean compareFlightPath(FlightPathType a, FlightPathType b) {
        AirportType aFrom = a.getFrom();
        AirportType aTo = a.getTo();
        
        AirportType bFrom = b.getFrom();
        AirportType bTo = b.getTo();
        return compareAirportType(aFrom, bFrom) && compareAirportType(aTo, bTo);
    }
    
    public static boolean compareAirportType(AirportType a, AirportType b) {
        String aCode = a.getCode();
        String bCode = b.getCode();
        
        return aCode.equals(bCode);
    }
}
